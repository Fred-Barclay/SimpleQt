# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'simple.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.buttonShowError = QtWidgets.QPushButton(self.centralwidget)
        self.buttonShowError.setObjectName("buttonShowError")
        self.gridLayout.addWidget(self.buttonShowError, 1, 1, 1, 1)
        self.pushButton_2 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_2.setObjectName("pushButton_2")
        self.gridLayout.addWidget(self.pushButton_2, 2, 1, 1, 1)
        self.buttonShowInfo = QtWidgets.QPushButton(self.centralwidget)
        self.buttonShowInfo.setObjectName("buttonShowInfo")
        self.gridLayout.addWidget(self.buttonShowInfo, 0, 1, 1, 1)
        self.buttonAskQ = QtWidgets.QPushButton(self.centralwidget)
        self.buttonAskQ.setObjectName("buttonAskQ")
        self.gridLayout.addWidget(self.buttonAskQ, 0, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.buttonShowError.setText(_translate("MainWindow", "Show Error"))
        self.pushButton_2.setText(_translate("MainWindow", "PushButton"))
        self.buttonShowInfo.setText(_translate("MainWindow", "Show Info"))
        self.buttonAskQ.setText(_translate("MainWindow", "Ask Question"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

