#!/usr/bin/env python3
'''Simple gui for testing Qt dialogs and features quickly.'''
import sys
from PyQt5 import QtWidgets, QtCore, QtGui
from simpleui import Ui_MainWindow
from notifiCat import *

class Gui(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.buttonAskQ.clicked.connect(self.askQ)
        self.ui.buttonShowInfo.clicked.connect(self.showInfo)
        self.ui.buttonShowError.clicked.connect(self.showError)


    def showError(self):
        msg = 'This is an error.'
        errorNotif(self, msg)

    def showInfo(self):
        title = 'SimpleQt Info'
        msg = 'This is sample information.'
        infoNotif(self, title, msg)


    def askQ(self):
        print('askQ')
        title = 'SimpleQt Question'
        msg = 'Is this a question?'
        q = askQuestion(self, title, msg)
        # askQuestion(self)

def main():
    app=QtWidgets.QApplication.instance()

    if not app:
        app = QtWidgets.QApplication(sys.argv)

    ex1 = Gui()
    ex1.setWindowTitle('SimpleQt')
    ex1.raise_()
    ex1.show()
    ex1.activateWindow()
    app.exec_()
    sys.exit(0)

if __name__ == '__main__':
    main()
