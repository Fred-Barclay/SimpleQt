#import tkMessageBox
#from Tkinter import Tk
from PyQt5.QtWidgets import QWidget, QMessageBox, QPushButton
from PyQt5 import QtCore

# Meow
'''
Use me to display notifications. I can be used in multiple functions
simultaneously so you don't end up with redundant/conflicting code.
'''

def errorNotif(self, msg):
    '''Display an error or warning message.'''
    QMessageBox.warning(self, 'Error', '\n'+msg+'\n')

#
def infoNotif(self, title, msg):
    '''Display a notification message.'''

    QMessageBox.information(self, title, '\n'+msg+'\n')

def askQuestion(self, title, msg):
    '''Ask a question with a yes/no dialog.'''
    # We can't use QMessageBox.question because it creates a modal dialog (i.e
    # it blocks viewing/interacting with other RespiRate windows). This means
    # that a user would not be able to check the data graphs until after
    # choosing whether to export the data.

    # ask = QMessageBox.question(self, title, '\n'+msg+'\n', QMessageBox.Yes | QMessageBox.No, QMessageBox.Yes)
    ask = QMessageBox(self)
    ask.setWindowTitle(title)
    ask.setText(msg)
    ask.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
    ask.setDefaultButton(QMessageBox.Yes)
    # ask.addButton(QPushButton('Yes'), QMessageBox.YesRole)
    print(str(ask))


    if ask == QMessageBox.Yes:
        print('YES')
        ans = 'yes'
    else:
        print('NO')
        ans = 'no'
    ask.exec()
    ask.setModal(0)
    ask.show()
    print(ans)
    # return(ans)
